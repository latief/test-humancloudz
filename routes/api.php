<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'dropdown'], function () {
    Route::get('province', [
        'as' => 'api.dropwdown.province',
        'uses' => 'Api\Dropdown\ProvinceController@index'
    ]);
    Route::post('city', [
        'as' => 'api.dropwdown.city',
        'uses' => 'Api\Dropdown\CityController@index'
    ]);
});