@extends('layouts.app')

@section('css')
<!-- sweetalert2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/8.11.8/sweetalert2.min.css" rel="stylesheet" type="text/css" />

<!-- select2 -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.4/css/select2.min.css" rel="stylesheet" />
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="form-group">
                        <label>Province</label>
                        <select id="hcl-province-select" class="form-control select2"></select>
                    </div>

                    <div class="form-group">
                        <label>City</label>
                        <select id="hcl-city-select" class="form-control select2"></select>
                    </div>

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

<script type="text/javascript">
    $(document).ready(function(e) {

        var APP_URL = {!! json_encode(url('/')) !!};
        $(".select2").select2();

        let dropdownCitySelect = $('#hcl-city-select');
            dropdownCitySelect.empty();

        try {
            let dropdownProvinceSelect = $('#hcl-province-select');
            dropdownProvinceSelect.empty();
            const dropdownProvinceSelectUrl = APP_URL +'/api/dropdown/province';

            $.getJSON(dropdownProvinceSelectUrl, function(data) {
                var dataProvince = data.data;
                $.each(dataProvince, function(key, entry) {
                    dropdownProvinceSelect.append($('<option></option>').attr('value', entry.id).text(entry.text));
                })

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'POST',
                    url: APP_URL +'/api/dropdown/city',
                    data: { ProvinceID: dropdownProvinceSelect.val() },
                    success: function (data) {
                        var dataCity = data.data;
                        dropdownCitySelect.empty();
                        $.each(dataCity, function(key, entry) {
                            dropdownCitySelect.append($('<option></option>').attr('value', entry.id).text(entry.text));
                        })
                    },
                    error: function(xhr, status, error) {
                        var err = eval("(" + xhr.responseText + ")");
                        Swal.fire({
                            html: '<strong>Oops!</strong> ' +err.Message
                        });
                    }
                });
            });
        }
        catch(err) {
            Swal.fire({
                html: '<strong>Oops!</strong> ' +err.Message
            });
        }


        // change drop down province
        $('#hcl-province-select').on('select2:select', function (e) {
            var data = e.params.data;
            var idProvince = data.id;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: 'POST',
                url: APP_URL +'/api/dropdown/city',
                data: { ProvinceID: idProvince },
                success: function (data) {
                    var dataCity = data.data;
                    dropdownCitySelect.empty();
                    $.each(dataCity, function(key, entry) {
                        dropdownCitySelect.append($('<option></option>').attr('value', entry.id).text(entry.text));
                    })
                },
                error: function(xhr, status, error) {
                    var err = eval("(" + xhr.responseText + ")");
                    Swal.fire({
                        html: '<strong>Oops!</strong> ' +err.Message
                    });
                }
            });
        });
    });
</script>
@endsection