<?php

namespace App\Http\Controllers\Api\Dropdown;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProvinceController extends Controller
{
    public function index(Request $request)
    {	
    	try {

            $headers = array('Content-Type' => 'application/json');
            $fetch = \Requests::post(
                config('humancloudz.resource.url.province'), 
                $headers, 
                []
            );
            
            if (201 == $fetch->status_code) {
                if (true == app('string.helper')->isJson($fetch->body)) {
                    if (true == app('humancloudz.helper')->ValidateResponseProvince($fetch->body)) {
                    	$bodyResponseArray = json_decode($fetch->body, true);
                    	$response = app('humancloudz.helper')->transformDataResponseProvince($bodyResponseArray['Value']);
                    	return response()->json($response);
                    }
                }
            }
        } catch (\Exception $e) {
            return response()->json([
            	'status' => false,
            	'data' => [],
            	'error' => [
            		'message' => $e->getMessage()
            	]
			]);
        }
    }
}
