<?php

namespace App\Http\Controllers\Api\Dropdown;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    public function index(Request $request)
    {
    	try {

            if (is_null($request->ProvinceID)) {
            	abort(400, 'Paramater ProvinceID is required');
            }

            $headers = array('Content-Type' => 'application/json');
            $fetch = \Requests::post(
                config('humancloudz.resource.url.city'), 
                $headers, 
                json_encode(['ProvinceID' => $request->ProvinceID])
            );

            if (201 == $fetch->status_code) {
            	if (true == app('string.helper')->isJson($fetch->body)) {
            		if (true == app('humancloudz.helper')->ValidateResponseCity($fetch->body)) {
            			$bodyResponseArray = json_decode($fetch->body, true);
            			$response = app('humancloudz.helper')->transformDataResponseCity($bodyResponseArray['Value']);
                    	return response()->json($response);
            		}
            	}
            }

        } catch (\Exception $e) {
            return response()->json([
            	'status' => false,
            	'data' => [],
            	'error' => [
            		'message' => $e->getMessage()
            	]
			]);
        }
    }
}
