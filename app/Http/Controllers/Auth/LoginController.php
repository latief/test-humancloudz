<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        $this->validateLogin($request);


        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            return $this->sendLockoutResponse($request);
        }


        try {
            $headers = array('Content-Type' => 'application/json');
            $fetch = \Requests::post(
                config('humancloudz.resource.url.login'), 
                $headers, 
                json_encode(config('humancloudz.resource.request-data.login'))
            );
            if (201 == $fetch->status_code) {
                if (true == app('string.helper')->isJson($fetch->body)) {
                    if (true == app('humancloudz.helper')->ValidateResponseLogin($fetch->body)) {
                        return redirect('/home');
                    }
                }
            }
        } catch (\Exception $e) {
            return $this->sendFailedLoginResponse($request);
        }
    }
}
