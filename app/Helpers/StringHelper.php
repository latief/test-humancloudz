<?php 

namespace App\Helpers;

use Illuminate\Support\Str;

class StringHelper
{
	public function isJson($string)
	{
		return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;	
	}
}