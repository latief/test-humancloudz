<?php 

namespace App\Helpers;

use Illuminate\Support\Str;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;

class HumancloudzHelper
{
	public function ValidateResponseLogin($response)
	{
		$isValid = true;
		$responseArray = json_decode($response, true);
		$listKeyValid = config('humancloudz.resource.key-response.login');

		foreach ($listKeyValid as $key => $value) {
			$keyExists = array_key_exists($value, $responseArray);
			if (false == $keyExists) {
				return false;
			}
		}

		return $isValid;
	}

	public function ValidateResponseProvince($response)
	{
		$isValid = true;
		$responseArray = json_decode($response, true);
		$listKeyValid = config('humancloudz.resource.key-response.province');
		$listKeyValidProvinceChild = config('humancloudz.resource.key-response.province-child-value');

		foreach ($listKeyValid as $key => $value) {
			$keyExists = array_key_exists($value, $responseArray);
			if (false == $keyExists) {
				return false;
			}
		}

		$dataProvince = $responseArray['Value'];
		if (count($dataProvince) < 1)
			return false;

		$oneDataProvince = $dataProvince[0];
		foreach ($oneDataProvince as $key => $value) {
			$keyExists = in_array($key, $listKeyValidProvinceChild);			
			if (false == $keyExists) {
				return false;
			}
		}

		return $isValid;
	}

	public function ValidateResponseCity($response)
	{
		$isValid = true;
		$responseArray = json_decode($response, true);
		$listKeyValid = config('humancloudz.resource.key-response.city');
		$listKeyValidCityChild = config('humancloudz.resource.key-response.city-child-value');

		foreach ($listKeyValid as $key => $value) {
			$keyExists = array_key_exists($value, $responseArray);
			if (false == $keyExists) {
				return false;
			}
		}

		$dataCity = $responseArray['Value'];
		if (count($dataCity) < 1)
			return false;

		$oneDataCity = $dataCity[0];
		foreach ($oneDataCity as $key => $value) {
			$keyExists = in_array($key, $listKeyValidCityChild);			
			if (false == $keyExists) {
				return false;
			}
		}

		return $isValid;
	}

	public function transformDataResponseProvince($dataProvinces)
	{
		$fractal = new Manager();
		$resource = new Collection($dataProvinces, function(array $province) {
		    return [
				'id' => $province['ProvinceID'],
		    	'text' =>$province['ProvinceName']
		    ];
		});

		return $fractal->createData($resource)->toArray();
	}

	public function transformDataResponseCity($dataCities)
	{
		$fractal = new Manager();
		$resource = new Collection($dataCities, function(array $city) {
		    return [
				'id' => $city['CityID'],
		    	'text' =>$city['CityName']
		    ];
		});

		return $fractal->createData($resource)->toArray();
	}
}