<?php

return [
	'url' => [
		'login' => 'http://35.240.192.212:8997/InventFoodCulinary/service/login',
		'province' => 'http://35.240.192.212:8997/InventFoodCulinary/service/getprovince',
		'city' => 'http://35.240.192.212:8997/InventFoodCulinary/service/getcity'
	],
	'request-data' => [
		'login' => [
			'Username' => 'Budi',
			'Password' => '123456',
			'Token' => 1234567890,
			'OsType' => 'ANDROID',
			'Role' => 'Customer'
		]
	],
	'key-response' => [
		'login' => [
			'Title',
			'StatusCode',
			'StatusMessage'
		],
		'province' => [
			'Title',
			'StatusCode',
			'StatusMessage',
			'Value'
		],
		'city' => [
			'Title',
			'StatusCode',
			'StatusMessage',
			'Value'
		],
		'province-child-value' => [
			'ProvinceID',
			'ProvinceName'
		],
		'city-child-value' => [
			'CityID',
			'CityName',
			'ProvinceID'
		]
	]
];